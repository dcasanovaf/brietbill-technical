package com.britebill.interview.transformers;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:context.xml"})
public class TransformerDataTest {

    @Autowired
    TransformerData transformerData;

    @Test
    public void testTransformer() {
        ArrayList<String> src = new ArrayList<String>(Arrays.asList("test","t","te","t"));
        List<String> transformData = transformerData.transformData(src);

        Assert.assertEquals(transformData.size(), 3L);
        Assert.assertEquals(transformData.get(0), "T");
    }
}
