package com.britebill.interview.transformers;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TransformerData {
    public List<String> transformData (List<String> src) {
        src.replaceAll(String::toUpperCase);
        return src.stream().sorted(Comparator.comparing(String::length)).distinct().collect(Collectors.toList());
    }
}
