package com.britebill.interview.statistics.writers;

import com.britebill.interview.statistics.beans.Statistics;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.IOException;


public class XmlStatisticsWriter implements StatisticsWriter {

    public void write(Statistics statistics, File file) {
        XmlMapper mapper = new XmlMapper();

        try {
            mapper.writeValue(file, statistics);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
