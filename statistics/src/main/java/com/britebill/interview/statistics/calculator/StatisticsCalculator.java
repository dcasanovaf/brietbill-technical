package com.britebill.interview.statistics.calculator;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class StatisticsCalculator {

    public static int getTotalNumberOfWords(List<String> data) {
        return data.size();
    }

    public static int getTotalNumberOfUniqueWords(List<String> data) {
        return data.stream().distinct().collect(Collectors.toList()).size();
    }

    public static int getAverageCharactersPerWord(List<String> data) {
        long count = data.stream().map(s->s.split("")).flatMap(Arrays::stream).count();
        return (int)count/data.size();
    }

    public static String getMostRepeatedWord(List<String> data) {
        return data.stream().collect(Collectors.groupingBy(w -> w, Collectors.counting())).entrySet().stream()
                .max(Comparator.comparing(Map.Entry::getValue)).get().getKey();
    }
}
