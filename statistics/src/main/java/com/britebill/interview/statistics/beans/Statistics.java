package com.britebill.interview.statistics.beans;

public class Statistics {
    private int totalNumberOfWords;
    private int totalNumberOfUniqueWords;
    private int averageCharactersPerWord;
    private String mostRepeatedWord;

    public int getTotalNumberOfWords() {
        return totalNumberOfWords;
    }

    public void setTotalNumberOfWords(int totalNumberOfWords) {
        this.totalNumberOfWords = totalNumberOfWords;
    }

    public int getTotalNumberOfUniqueWords() {
        return totalNumberOfUniqueWords;
    }

    public void setTotalNumberOfUniqueWords(int totalNumberOfUniqueWords) {
        this.totalNumberOfUniqueWords = totalNumberOfUniqueWords;
    }

    public int getAverageCharactersPerWord() {
        return averageCharactersPerWord;
    }

    public void setAverageCharactersPerWord(int averageCharactersPerWord) {
        this.averageCharactersPerWord = averageCharactersPerWord;
    }

    public String getMostRepeatedWord() {
        return mostRepeatedWord;
    }

    public void setMostRepeatedWord(String mostRepeatedWord) {
        this.mostRepeatedWord = mostRepeatedWord;
    }
}
