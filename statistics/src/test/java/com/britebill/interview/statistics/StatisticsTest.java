package com.britebill.interview.statistics;

import com.britebill.interview.statistics.beans.Statistics;
import com.britebill.interview.statistics.calculator.StatisticsCalculator;
import com.britebill.interview.statistics.writers.JsonStatisticsWriter;
import com.britebill.interview.statistics.writers.XmlStatisticsWriter;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:context.xml"})
public class StatisticsTest {

    @Autowired
    @Qualifier(value = "xmlTextStatistics")
    private XmlStatisticsWriter xmlStatisticsWriter;

    @Autowired
    @Qualifier(value = "jsonTextStatistics")
    private JsonStatisticsWriter jsonStatisticsWriter;

    private ArrayList<String> src = new ArrayList<String>(Arrays.asList("test","t","te","t"));

    @Test
    public void testXmlGeneration() {
        xmlStatisticsWriter.write(new Statistics(), new File("output-test.xml"));
        File file = new File("output-test.xml");

        Assert.assertNotNull(file);
    }

    @Test
    public void testJsonGeneration() {
        jsonStatisticsWriter.write(new Statistics(), new File("output-json-test.xml"));
        File file = new File("output-json-test.xml");

        Assert.assertNotNull(file);
    }

    @Test
    public void testTotalNumberOfWords() {
        Assert.assertEquals(StatisticsCalculator.getTotalNumberOfWords(src), 4L);
    }

    @Test
    public void testTotalNumberOfUniqueWords() {
        Assert.assertEquals(StatisticsCalculator.getTotalNumberOfUniqueWords(src), 3L);
    }

    @Test
    public void testAverageCharactersPerWord() {
        Assert.assertEquals(StatisticsCalculator.getAverageCharactersPerWord(src), 2L);
    }

    @Test
    public void testMostRepeatedWord() {
        Assert.assertEquals(StatisticsCalculator.getMostRepeatedWord(src), "t");
    }

}
